﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(WheelJoint2D))]
public class CarController : MonoBehaviour {

	#region variables
	public Transform carBodyParent;
	public AudioSource engineAudioSource;

	#region 2D Physics variables
	public WheelJoint2D jointFront, jointRear, jointMiddle;
	[HideInInspector]
	public Rigidbody2D rigidbodyComponent;
	[HideInInspector]
	public JointSuspension2D suspensionFront, suspensionRear, suspensionMiddle;
	[HideInInspector]
	public JointMotor2D motorFront, motorRear, motorMiddle;
	[HideInInspector]
	public Wheel wheelFront, wheelRear, wheelMiddle;
	public DistanceJoint2D distanceFront, distanceRear, distanceMiddle;
	public DistanceJoint2D distanceMaxfront, distanceMaxRear;
	#endregion

	#region Rocket variables
	public ParticleSystem thrusterParticle;
	private float thrusterTimer = -1;
	private float thrusterDuration = 3f;
	private float thrusterForceAmount = 10f;
	private int thrusterParticleAmount = 500;
	#endregion

	#region Interactable variables
	private float acceleratorAmount = 50;
	private float health = 100;
	public float fuel { get; set; }
	private float maxFuel = 20;
	private float fuelRate = 0.5f;
	#endregion

	[HideInInspector]
	public float flipAdjustment = 500;

	#region Motor variables
	[HideInInspector]
	public float motorSpeed = 10000;
	private float maxMotorTorque = 100000;
	private bool lockMotorRearFlag = false;
	private float carAngle, groundAngle;
	[HideInInspector]
	public float engineLevel;
	#endregion

	#region Wheel variables
	private float wheelRadius = 1f;
	private Vector3 wheelFrontOffset = new Vector3(2.778f, -1f, 0.1f);
	private Vector3 wheelRearOffset = new Vector3(-2.778f, -1f, 0.1f);
	private float baseFriction, maxFriction, maxFrictionLow, maxFrictionHigh;
	[HideInInspector]
	public float frictionLevel;
	#endregion

	#region Body variables
	private float freeRotationAmount = 60f;
	private float currentFreeRotate;
	private float freeRotateMultiplier;
	private float rotateMultiplier = 1;
	[HideInInspector]
	public float balanceLevel, flipAssistLevel;
	[HideInInspector]
	public bool bodyFlipped = false;
	private Vector2 downForceFront, downForceRear;
	private float downForceMultiplier;
	#endregion

	#region Accelerator variables
	[HideInInspector]
	public bool wheelFrontAccelerator = false, wheelRearAccelerator = false;
	#endregion

	#region Roller Coaster variables
	[HideInInspector]
	public bool usingRollerCoaster = false;
	private float rollerCoasterBoost = 1500f;
	[HideInInspector]
	public Transform rollerCoasterCenter;
	[HideInInspector]
	public bool rollerCoasterDownwardFlag = false;
	#endregion

	public float inputX { get; set; }
	#endregion

	#region MonoBehaviour
	void Awake () {
		AssignVariables ();
	}

	// Use this for initialization
	void Start () {
		InitializeVariables ();
		transform.gameObject.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
		inputX = (fuel <= 0? 0 : UserInputController.instance.inputX);

		UpdateUI ();

		UpdateFuel ();
	}

	void FixedUpdate () {

		UpdateMotor ();

		//UpdateSuspension ();

		UpdateBody ();

		UseRocket ();

		//UseRollerCoaster ();

		//UpdateWheels ();

		//UpdateFriction ();

		UseAccelerator ();
	}
	#endregion

	#region Methods
	private void AssignVariables () {
		rigidbodyComponent = GetComponent<Rigidbody2D> ();
		suspensionFront = jointFront.suspension;
		suspensionRear = jointRear.suspension;
		suspensionMiddle = jointMiddle.suspension;
		motorFront = jointFront.motor;
		motorRear = jointRear.motor;
		motorMiddle = jointMiddle.motor;
		wheelFront = jointFront.connectedBody.GetComponent<Wheel> ();
		wheelRear = jointRear.connectedBody.GetComponent<Wheel> ();
		wheelMiddle = jointMiddle.connectedBody.GetComponent<Wheel> ();
	}

	private void InitializeVariables () {
		rigidbodyComponent.centerOfMass = new Vector3 (0, -0.5f, 0);
		suspensionFront.angle = suspensionRear.angle = suspensionMiddle.angle = 90;
		jointFront.suspension = suspensionFront;
		jointRear.suspension = suspensionRear;
		jointMiddle.suspension = suspensionMiddle;

		baseFriction = 1f;
		// max friction is dynamic
		maxFrictionLow = baseFriction * 2f;
		maxFrictionHigh = maxFrictionLow * 2f;

		fuel = maxFuel;
		fuelRate = Mathf.Lerp (0.5f, 0.2f, engineLevel);
	}

	#region Motor methods
	private void EngineRWD (float speed, float torque) {
		UpdateCarBodyOrientation ();

		// lock motor when extreme landing, assuming wheel is being pushed to either end and body's normal is largely different from ground.
		if ((jointRear.jointTranslation > wheelRear.GetComponent<CircleCollider2D>().radius * 0.8f || jointRear.jointTranslation < wheelRear.GetComponent<CircleCollider2D>().radius * 0.2f) && !wheelFront.grounded && !lockMotorRearFlag && Mathf.Abs(carAngle - groundAngle) > (90f - 60f * flipAssistLevel) && !usingRollerCoaster)
			lockMotorRearFlag = true;

		// release motor lock
		if (lockMotorRearFlag && wheelFront.grounded && wheelRear.grounded)
			lockMotorRearFlag = false;

		// case: going forward and down slope
		if (groundAngle < 0 && inputX > 0 && wheelRear.grounded) {

			// Angular acceleration at center of mass of rotating object down slope = (g * sin (theta)) / (1 + moment of inertia / (m * r^2))
			// https://www.youtube.com/watch?v=gj9H1Ti4yc0
			// moment of inertia of disk = (1/4) * (m * r^2)
			// http://hyperphysics.phy-astr.gsu.edu/hbase/tdisc.html
			// my testing suggest acceleration need to be further divided by radius and moment of inertia to be (2/3) * (m * r^2)
			float spinFromGravityPull = (2f * 9.81f * Mathf.Sin (-groundAngle * Mathf.Deg2Rad)) / (3f * wheelRear.GetComponent<CircleCollider2D> ().radius) * Mathf.Rad2Deg * Time.fixedDeltaTime;
			motorRear.motorSpeed = Mathf.Clamp (motorRear.motorSpeed - spinFromGravityPull, float.NegativeInfinity, speed);
		}
		else {
			if ((motorRear.motorSpeed > 0 && speed > 0) || (motorRear.motorSpeed < 0 && speed < 0)) {
				motorRear.motorSpeed = Mathf.Lerp (motorRear.motorSpeed, speed, Time.fixedDeltaTime);
			} else {
				motorRear.motorSpeed = speed;
			}
		}
		motorRear.maxMotorTorque = torque;

		if (lockMotorRearFlag) {
			motorRear.motorSpeed = 0;
			motorRear.maxMotorTorque = 0;
		}

		// wheel middle and rear share same properties
		if (jointMiddle.enabled) {
			motorMiddle.motorSpeed = motorRear.motorSpeed;
			motorMiddle.maxMotorTorque = motorRear.maxMotorTorque;
		}
	}

	private void EngineFWD (float speed, float torque) {
		// See RWD
		if (groundAngle < 0 && inputX > 0 && wheelFront.grounded) {
			float spinFromGravityPull = (2f * 9.81f * Mathf.Sin (-groundAngle * Mathf.Deg2Rad)) / (3f * wheelFront.GetComponent<CircleCollider2D> ().radius) * Mathf.Rad2Deg * Time.fixedDeltaTime;
			motorFront.motorSpeed = Mathf.Clamp (motorFront.motorSpeed - spinFromGravityPull, float.NegativeInfinity, speed);
		}
		else {
			if ((motorFront.motorSpeed > 0 && speed > 0) || (motorFront.motorSpeed < 0 && speed < 0)) {
				motorFront.motorSpeed = Mathf.Lerp (motorFront.motorSpeed, speed, Time.fixedDeltaTime);
			} else {
				motorFront.motorSpeed = speed;
			}
		}
		motorFront.maxMotorTorque = torque;
	}

	private void Engine4WD (float speed, float torque) {
		EngineRWD (speed / 2f, torque);
		EngineFWD (speed / 2f, torque);
	}

	private void UpdateMotor () {
		float speed = -inputX * motorSpeed;
		float torque = (inputX == 0 ? 0 : maxMotorTorque);

		switch (ConfigController.instance.GetCurrentConfig().engine) {
		case ConfigController.EngineType._RWD:
			EngineRWD (speed, torque);
			break;
		case ConfigController.EngineType._FWD:
			EngineFWD (speed, torque);
			break;
		case ConfigController.EngineType._4WD:
			Engine4WD (speed, torque);
			break;
		}

		jointRear.motor = motorRear;
		jointFront.motor = motorFront;
		jointMiddle.motor = motorMiddle;
	}
	#endregion

	private void UpdateSuspension () {
		if (!wheelFront.grounded && !wheelRear.grounded) {
			suspensionFront.frequency = Mathf.Lerp (suspensionFront.frequency, 4, Time.deltaTime);
			suspensionRear.frequency = Mathf.Lerp (suspensionRear.frequency, 4, Time.deltaTime);			
		} else {
			if (!wheelFront.grounded)
				suspensionRear.frequency = Mathf.Lerp (suspensionRear.frequency, 1, Time.deltaTime);
			else
				suspensionRear.frequency = Mathf.Lerp (suspensionRear.frequency, 4, Time.deltaTime);
			if (!wheelRear.grounded)
				suspensionFront.frequency = Mathf.Lerp (suspensionFront.frequency, 1, Time.deltaTime);
			else
				suspensionFront.frequency = Mathf.Lerp (suspensionFront.frequency, 4, Time.deltaTime);
		}			

		jointFront.suspension = suspensionFront;
		jointRear.suspension = suspensionRear;
	}

	private void UpdateBody () {
		if (usingRollerCoaster)
			return;

		// add downforce when mid-air to make the car drop faster
		if (!wheelFront.grounded && !wheelRear.grounded) {
			rigidbodyComponent.AddForce (rigidbodyComponent.velocity.magnitude * rigidbodyComponent.mass * Vector2.down.normalized);
		}

		// if the car is not flipped, car is moving forward but front wheel if lifted
		// add force to flip backward according to flipAssitLevel, upgrade to make more difficult to flip
		if (!bodyFlipped && !wheelFront.grounded && motorRear.motorSpeed < 0 && carAngle >= 0 && inputX != 0) {
			rigidbodyComponent.AddTorque (rigidbodyComponent.mass * Mathf.Lerp (1f, 0f, flipAssistLevel));
		}


		if (!wheelFront.grounded && !wheelRear.grounded) {
			currentFreeRotate = Mathf.Lerp (currentFreeRotate, freeRotationAmount, Time.fixedDeltaTime);

			//freeRotateMultiplier = Mathf.Pow (Mathf.Abs (_rigidbody.angularVelocity), 0.25f);
			freeRotateMultiplier = 1;
			if ((inputX > 0 && rigidbodyComponent.angularVelocity < 0) || (inputX < 0 && rigidbodyComponent.angularVelocity > 0))
				//freeRotateMultiplier = Mathf.Abs(_rigidbody.angularVelocity);
				freeRotateMultiplier = 10;
			freeRotateMultiplier = Mathf.Clamp (freeRotateMultiplier, 1, 180);

			if (inputX != 0 && !bodyFlipped) {
				rigidbodyComponent.AddTorque (inputX * currentFreeRotate * freeRotateMultiplier * Mathf.Clamp(rigidbodyComponent.velocity.magnitude, 1f, float.PositiveInfinity));
				if (inputX < 0) {
					Vector2 velocity = rigidbodyComponent.velocity;
					velocity.x *= 1f - (0.1f * Time.fixedDeltaTime);
					rigidbodyComponent.velocity = velocity;
				}
			} else {
				//_rigidbody.angularVelocity = Mathf.Lerp (_rigidbody.angularVelocity, 0, Time.fixedDeltaTime * 5f);
				rigidbodyComponent.angularVelocity *= 1f - (0.1f * Time.fixedDeltaTime);
			}
		} else {
			currentFreeRotate = Mathf.Lerp (currentFreeRotate, 0, Time.fixedDeltaTime);

			if (!wheelFront.grounded && carAngle > 0) {
				rotateMultiplier = Mathf.Lerp(rotateMultiplier, rotateMultiplier + (lockMotorRearFlag? Mathf.Clamp(Mathf.Abs(carAngle - groundAngle), 1, 180) : 0), Time.fixedDeltaTime);
				rigidbodyComponent.AddTorque ((carAngle < 90? 1 : -1) * -rigidbodyComponent.mass / 50f * Mathf.Clamp(Mathf.Abs(rigidbodyComponent.velocity.magnitude), 1f, float.MaxValue) * Mathf.Clamp (Mathf.Abs (rigidbodyComponent.angularVelocity), 1, 180) * rotateMultiplier * flipAssistLevel);
			} else if (!wheelRear.grounded && carAngle < 0) {
				//rotateMultiplier = Mathf.Lerp(rotateMultiplier, rotateMultiplier + 1, Time.fixedDeltaTime);
				rigidbodyComponent.AddTorque (rigidbodyComponent.mass / 20f * Mathf.Clamp(Mathf.Abs(rigidbodyComponent.velocity.magnitude), 1f, float.MaxValue) * Mathf.Clamp (Mathf.Abs (rigidbodyComponent.angularVelocity), 1, 180) * flipAssistLevel);
			} else
				rotateMultiplier = Mathf.Lerp(rotateMultiplier, 1, Time.fixedDeltaTime * 10);

			// downforce proportional to mass and velocity applied to each wheel, keep the car close to ground
			// slopeMultiplier compensate gravity pull
			float slopeMultiplier = 1f;
			float angle = (Vector3.Cross(Vector3.up, transform.up).z < 0? -1 : 1) * Vector3.Angle (Vector3.up, transform.up);
			if (angle > 0 && angle < 60)
				slopeMultiplier = Mathf.Clamp(Mathf.Pow(Mathf.Clamp(1f / Mathf.Abs(Mathf.Cos(Mathf.Deg2Rad * angle)), 1f, 5f), 5f), 1f, 30f);
			slopeMultiplier = Mathf.Lerp (1f, slopeMultiplier, engineLevel);

			downForceMultiplier = 1f;
			if (motorRear.motorSpeed != 0f)
				downForceMultiplier = Mathf.Clamp (Mathf.Abs(motorRear.motorSpeed) / rigidbodyComponent.velocity.magnitude, 1f, 1000f) * slopeMultiplier;
			if (wheelFront.grounded && inputX != 0f) {
				//downForceFront = _rigidbody.mass * Mathf.Pow (Mathf.Clamp (Mathf.Abs (_rigidbody.velocity.magnitude), 1f, float.MaxValue), 1.05f) * -wheelFront.contactNormal * slopeMultiplier * flipAssistLevel;
				downForceFront = -wheelFront.contactNormal * downForceMultiplier * flipAssistLevel;
				rigidbodyComponent.AddForceAtPosition (downForceFront, wheelFront.transform.position);
			}
			if (wheelRear.grounded && inputX != 0f) {
				//downForceRear = _rigidbody.mass * Mathf.Pow (Mathf.Clamp (Mathf.Abs (_rigidbody.velocity.magnitude), 1f, float.MaxValue), 1.05f) * -wheelRear.contactNormal * slopeMultiplier;
				downForceRear = -wheelRear.contactNormal *  downForceMultiplier;
				rigidbodyComponent.AddForceAtPosition (downForceRear, wheelRear.transform.position);
			}

			// slow down car when only one wheel grounded
			if (!wheelFront.grounded || !wheelRear.grounded)
				rigidbodyComponent.velocity *= 1f - (0.1f * Time.fixedDeltaTime);
		}

		rigidbodyComponent.angularVelocity = Mathf.Clamp (rigidbodyComponent.angularVelocity, -90f, 90f);
	}

	private void UpdateWheels () {
		/* world space implementation
		if (transform.InverseTransformPoint(wheelRear.transform.position).y < (wheelRearOffset.y - wheelRadius)) {
			Vector3 wheelMaxOffset = wheelRearOffset;
			wheelMaxOffset.y -= wheelRadius;
			wheelRear.transform.position = Vector3.Lerp(wheelRear.transform.position, transform.TransformPoint (wheelMaxOffset), 1f);
		}
		if (transform.InverseTransformPoint(wheelFront.transform.position).y < wheelFrontOffset.y - wheelRadius) {
			Vector3 wheelMaxOffset = wheelFrontOffset;
			wheelMaxOffset.y -= wheelRadius;
			wheelFront.transform.position = Vector3.Lerp (wheelFront.transform.position, transform.TransformPoint (wheelMaxOffset), 1f);
		}*/

		// local space implementation
		Vector3 localPos = wheelFront.transform.localPosition;
		localPos.x = 1.25f;
		localPos.y = Mathf.Clamp (localPos.y, -1f, 1f);
		//wheelFront.transform.localPosition = localPos;
		wheelFront.transform.Translate (localPos - wheelFront.transform.localPosition, Space.World);

		localPos = wheelRear.transform.localPosition;
		localPos.x = -1.25f;
		localPos.y = Mathf.Clamp (localPos.y, -1f, 1f);
		//wheelRear.transform.localPosition = localPos;
		wheelRear.transform.Translate (localPos - wheelRear.transform.localPosition, Space.World);

		localPos = wheelMiddle.transform.localPosition;
		localPos.x = 0;
		localPos.y = Mathf.Clamp (localPos.y, -1f, 1f);
		//wheelMiddle.transform.localPosition = localPos;
		wheelMiddle.transform.Translate(localPos - wheelMiddle.transform.localPosition, Space.World);
	}

	public void UpdateFriction () {
		if (wheelRear.relativeVelocity.sqrMagnitude <= 0)
			return;

		Vector2 bodyVelocity = rigidbodyComponent.velocity;
		Vector2 wheelVelocity = wheelRear.relativeVelocity;
		if ((wheelVelocity.x > 0 && bodyVelocity.x > 0) || (wheelVelocity.x < 0 && bodyVelocity.x < 0))
			bodyVelocity = Vector2.zero;

		float ratio = Mathf.Clamp01 (wheelVelocity.sqrMagnitude / Mathf.Pow (bodyVelocity.sqrMagnitude, 3f));
		maxFriction = Mathf.Lerp (maxFrictionLow, maxFrictionHigh, ratio);
		ConfigController.instance.wheelPhysics.friction = baseFriction + (maxFriction - baseFriction) * ratio * frictionLevel;
		wheelFront.GetComponent<CircleCollider2D> ().enabled = false;
		wheelRear.GetComponent<CircleCollider2D> ().enabled = false;
		wheelMiddle.GetComponent<CircleCollider2D> ().enabled = false;
		wheelFront.GetComponent<CircleCollider2D> ().enabled = true;
		wheelRear.GetComponent<CircleCollider2D> ().enabled = true;
		wheelMiddle.GetComponent<CircleCollider2D> ().enabled = true;
	}

	private void UpdateCarBodyOrientation () {
		int layer = 1 << LayerMask.NameToLayer ("Road");

		Vector2 hitFront, hitRear;
		RaycastHit2D hit = Physics2D.Raycast (wheelFront.transform.position, Vector2.down, 2, layer);
		if (hit.collider != null) {
			hitFront = hit.point;
			hit = Physics2D.Raycast (wheelRear.transform.position, Vector2.down, 2, layer);
			if (hit.collider != null) {
				hitRear = hit.point;
				groundAngle = AngleSigned (Vector3.right, new Vector3 (hitFront.x - hitRear.x, hitFront.y - hitRear.y, 0), Vector3.forward);
			}
		}

		carAngle = AngleSigned (Vector3.right, transform.right, Vector3.forward);
	}

	public void ChangeHealth (float value) {
		if (!GameController.instance.damageEnabled)
			return;

		health = Mathf.Clamp (health + value, 0, 100);
		UIController.instance.SetText (UIController.instance.healthText, "HP " + (int)health);
		UIController.instance.SetRectTransformSize (UIController.instance.hpBar, new Vector2 (health / 100f * 200f, UIController.instance.hpBar.sizeDelta.y));
		if (health <= 0) {
			if (!GameController.instance.exploded)
				GameController.instance.Explode (true);
		}
	}

	public void ChangeFuel (float value) {
		fuel = Mathf.Clamp (fuel + value, 0, maxFuel);
		UIController.instance.SetRectTransformSize (UIController.instance.fuelBar, new Vector2 (fuel / maxFuel * 200f, UIController.instance.fuelBar.sizeDelta.y));
		if (fuel <= 0) {
			
		}
	}

	#region Rocket methods
	public void PickupRocket () {
		thrusterTimer = Time.time + thrusterDuration;
	}

	private void UseRocket () {
		if (thrusterTimer > Time.time) {
			if (wheelFront.grounded || wheelRear.grounded) {
				Vector3 force = transform.right * (motorSpeed / 100);
				rigidbodyComponent.AddForce (force, ForceMode2D.Force);
			}
			if (thrusterParticle != null)
				thrusterParticle.Emit ((int)(thrusterParticleAmount * Time.fixedDeltaTime));
		}
	}
	#endregion

	public void UseAccelerator () {
		if (inputX >= 0)
			if (wheelFrontAccelerator || wheelRearAccelerator)
				rigidbodyComponent.AddForce (transform.right * (motorSpeed / 100) * rigidbodyComponent.mass, ForceMode2D.Force);
	}

	#region Roller Coaster methods
	public void EnterRollerCoaster (bool b, Transform t) {
		usingRollerCoaster = b;
		rollerCoasterCenter = t;
	}

	private void UseRollerCoaster () {
		if (!usingRollerCoaster) {
			Vector3 targetPos = transform.position;
			targetPos.z = 0;
			transform.position = Vector3.Lerp (transform.position, targetPos , Time.fixedDeltaTime);
			return;
		}
		if (inputX > 0) {
			rigidbodyComponent.AddForce (-transform.up * rollerCoasterBoost * Time.fixedDeltaTime / 1.5f, ForceMode2D.Impulse);

			float downwardMultiplier = (rollerCoasterDownwardFlag? 0.75f : 1f);
			rigidbodyComponent.AddForce (transform.right * rollerCoasterBoost * Time.fixedDeltaTime * downwardMultiplier, ForceMode2D.Impulse);
		}

		if (rollerCoasterCenter != null) {
			Vector3 carPos = transform.position;
			carPos.z = rollerCoasterCenter.position.z;
			Vector3 relativePos = rollerCoasterCenter.InverseTransformPoint (carPos);
			float angle = AngleSigned(rollerCoasterCenter.up, relativePos, rollerCoasterCenter.forward);
			transform.localPosition = Vector3.Lerp (transform.localPosition, new Vector3(transform.localPosition.x, transform.position.y, (angle / 180f) * 4f) , Time.fixedDeltaTime);
		}
	}

	public static float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n)
	{
		return Mathf.Atan2(
			Vector3.Dot(n, Vector3.Cross(v1, v2)),
			Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
	}
	#endregion

	private void UpdateUI () {
		float currentSpeed = Mathf.Abs (rigidbodyComponent.velocity.x * 3.6f);
		UIController.instance.SetText (UIController.instance.carSpeedText, currentSpeed.ToString ("0"));
		Vector3 pos = UIController.instance.carSpeedIndicator.transform.localPosition;
		pos.x = Mathf.Lerp (-98f, 98f, (currentSpeed / 200f));
		UIController.instance.carSpeedIndicator.transform.localPosition = pos;
	}

	private void UpdateFuel () {
		if (GameController.instance.startTime >= 0) {
			ChangeFuel (-Time.deltaTime * fuelRate * (inputX > 0? 1.5f : 1));
		}
	}
	#endregion
}
