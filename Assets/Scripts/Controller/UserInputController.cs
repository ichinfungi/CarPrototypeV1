﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class UserInputController : MonoBehaviour {

	public static UserInputController instance;

	[HideInInspector]
	public float inputX;
	private bool accelerating = false, decelerating = false;

	void Awake () {
		instance = this;
 	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.P))
			GameController.instance.PauseGame ();
		if (Input.GetKeyDown (KeyCode.R))
			GameController.instance.RestartGame ();

		inputX = 0;

		if (accelerating)
			inputX = 1f;

		if (decelerating)
			inputX = -1f;

		#if UNITY_EDITOR
		inputX = Input.GetAxis("Horizontal");
		#endif
	}

	public void Accelerate (bool b) {
		accelerating = b;
	}

	public void Decelerate (bool b) {
		decelerating = b;
	}
}
