﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class ConfigController : MonoBehaviour {

	public static ConfigController instance;

	public enum EngineType {
		_RWD,
		_FWD,
		_4WD
	};

	[System.Serializable]
	public class CarConfig {
		public string name;
		public EngineType engine;
		public float speed;
		public float suspension;
		public float weight;
		public float balance;
		public float gravity;
		public float friction;
		public float flipAdjust;

		public CarConfig () {}
		public CarConfig (CarConfig config) {
			name = config.name;
			engine = config.engine;
			speed = config.speed;
			suspension = config.suspension;
			weight = config.weight;
			balance = config.balance;
			gravity = config.gravity;
			friction = config.friction;
			flipAdjust = config.flipAdjust;
		}
	};

	public PhysicsMaterial2D wheelPhysics;

	private CarConfig defaultConfig, currentConfig;
	private int initialConfigIndex;

	void Awake () {
		instance = this;
	}

	// Use this for initialization
	void Start () {
		defaultConfig = new CarConfig ();
		currentConfig = new CarConfig ();
		InitializeCarConfig (defaultConfig);
		initialConfigIndex = PlayerPrefs.GetInt ("Config Index");
		LoadCarConfig (initialConfigIndex);
		ApplyCarConfig (currentConfig);

		for (int i = 1; i < UIController.instance.configSlot.options.Count; i++) {
			CarConfig temp = Serializer.Load<CarConfig> (Application.persistentDataPath + "/config_" + i);
			if (temp != null)
				UIController.instance.SetSelectableOption (UIController.instance.configSlot, i, "Config " + i + " " + temp.name);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void InitializeCarConfig (CarConfig config) {
		config.name = "";
		config.engine = (EngineType)UIController.instance.GetSelectableValue (UIController.instance.configEngine);
		config.speed = UIController.instance.GetSelectableValue (UIController.instance.configSpeed);
		config.suspension = UIController.instance.GetSelectableValue (UIController.instance.configSuspension);
		config.weight = UIController.instance.GetSelectableValue (UIController.instance.configWeight);
		config.balance = UIController.instance.GetSelectableValue (UIController.instance.configBalance);
		config.gravity = UIController.instance.GetSelectableValue (UIController.instance.configGravity);
		config.friction = UIController.instance.GetSelectableValue (UIController.instance.configFriction);
		config.flipAdjust = UIController.instance.GetSelectableValue (UIController.instance.configFlipAdjust);
	}

	void LoadCarConfig (int index) {
		PlayerPrefs.SetInt ("Config Index", index);
		UIController.instance.SetSelectableValue (UIController.instance.configSlot, index);

		if (Serializer.Load<CarConfig> (Application.persistentDataPath + "/config_-1") != null)
			currentConfig = Serializer.Load<CarConfig> (Application.persistentDataPath + "/config_-1");
		else
			currentConfig = Serializer.Load<CarConfig> (Application.persistentDataPath + "/config_" + index);
		
		if (currentConfig == null) {
			currentConfig = new CarConfig(defaultConfig);
		}
		ApplyCarConfig (currentConfig);
	}

	public void SaveCarConfig () {
		int configSlotValue = (int)UIController.instance.GetSelectableValue (UIController.instance.configSlot);
		Serializer.Save<CarConfig> (Application.persistentDataPath + "/config_" + configSlotValue, currentConfig);
		if (configSlotValue != 0) {
			UIController.instance.SetSelectableOption (UIController.instance.configSlot, configSlotValue, "Config " + configSlotValue + " " + currentConfig.name);
		}
	}

	public void DeleteConfig () {
		for (int i = 0; i < UIController.instance.configSlot.options.Count - 1; i++) {
			string path = Application.persistentDataPath + "/config_" + i;
			if (File.Exists (path))
				File.Delete (path);
		}
	}

	void ApplyCarConfig (CarConfig config) {
		UIController.instance.SetInputField (UIController.instance.configName, config.name);
		UIController.instance.SetSelectableValue (UIController.instance.configEngine, (int)config.engine);
		UIController.instance.SetSelectableValue (UIController.instance.configSpeed, config.speed);
		UIController.instance.SetSelectableValue (UIController.instance.configSuspension, config.suspension);
		UIController.instance.SetSelectableValue (UIController.instance.configWeight, config.weight);
		UIController.instance.SetSelectableValue (UIController.instance.configBalance, config.balance);
		UIController.instance.SetSelectableValue (UIController.instance.configGravity, config.gravity);
		UIController.instance.SetSelectableValue (UIController.instance.configFriction, config.friction);
		UIController.instance.SetSelectableValue (UIController.instance.configFlipAdjust, config.flipAdjust);
		ChangeConfigName (UIController.instance.configName);
		ChangeEngineType (UIController.instance.configEngine);
		ChangeSpeed (UIController.instance.configSpeed);
		ChangeSuspension (UIController.instance.configSuspension);
		ChangeWeight (UIController.instance.configWeight);
		ChangeBalance (UIController.instance.configBalance);
		ChangeGravity (UIController.instance.configGravity);
		ChangeFriction (UIController.instance.configFriction);
		ChangeFlipAdjust (UIController.instance.configFlipAdjust);
	}

	public void ResetCarConfig () {
		ApplyCarConfig (defaultConfig);
	}

	public CarConfig GetCurrentConfig () {
		return currentConfig;
	}

	public void ChangeConfigName (InputField target) {
		if (target == null)
			return;
		
		currentConfig.name = target.text;
	}

	public void ChangeEngineType (Dropdown target) {
		if (target == null)
			return;
		
		switch (target.value) {
		case 0:
			currentConfig.engine = EngineType._RWD;
			break;
		case 1:
			currentConfig.engine = EngineType._FWD;
			break;
		case 2:
			currentConfig.engine = EngineType._4WD;
			break;
		}
	}

	public void ChangeSpeed (Slider target) {
		if (target == null)
			return;
		
		GameController.instance.car.motorSpeed = target.value;
		//GameController.instance.car._rigidbody.drag = (1f - (target.value - target.minValue) / (target.maxValue - target.minValue)) / 10f;
		UIController.instance.SetText (UIController.instance.speedText, "Engine " + (int)((target.value - target.minValue) / (target.maxValue - target.minValue) * 10f) + "/10");
		currentConfig.speed = target.value;
		GameController.instance.car.engineLevel = (target.value - target.minValue) / (target.maxValue - target.minValue);
	}

	public void ChangeSuspension (Slider target) {
		if (target == null)
			return;
		
		UIController.instance.SetText (UIController.instance.suspensionText, "Suspension " + (int)((target.value - target.minValue) / (target.maxValue - target.minValue) * 10f) + "/10");
		currentConfig.suspension = target.value;

		GameController.instance.car.suspensionFront.dampingRatio = GameController.instance.car.suspensionRear.dampingRatio = GameController.instance.car.suspensionMiddle.dampingRatio = target.value;
		//GameController.instance.car.suspensionFront.frequency = GameController.instance.car.suspensionRear.frequency = GameController.instance.car.suspensionMiddle.frequency = Mathf.Lerp(4f, 12f, (1f - target.value));
		GameController.instance.car.jointFront.suspension = GameController.instance.car.suspensionFront;
		GameController.instance.car.jointRear.suspension = GameController.instance.car.suspensionRear;
		GameController.instance.car.jointMiddle.suspension = GameController.instance.car.suspensionMiddle;
	}

	public void ChangeWeight (Slider target) {
		if (target == null)
			return;
		
		UIController.instance.SetText (UIController.instance.weightText, "Chassis " + (int)((target.maxValue - target.value) / (target.maxValue - target.minValue) * 10f) + "/10");
		currentConfig.weight = target.value;

		GameController.instance.car.rigidbodyComponent.mass = target.value;
	}

	public void ChangeBalance (Slider target) {
		if (target == null)
			return;
		
		UIController.instance.SetText (UIController.instance.balanceText, "Balance " + (int)(target.value * 10f) + "/10");
		currentConfig.balance = target.value;

		GameController.instance.car.balanceLevel = target.value;
		//CarSelfBalancing.instance.balanceAmount = target.value;
	}

	public void ChangeGravity (Slider target) {
		if (target == null)
			return;
		
		UIController.instance.SetText (UIController.instance.gravityText, "Gravity " + target.value.ToString ("0.00"));
		currentConfig.gravity = target.value;

		GameController.instance.car.rigidbodyComponent.gravityScale = target.value;
	}

	public void ChangeFriction (Slider target) {
		if (target == null)
			return;
		
		UIController.instance.SetText (UIController.instance.frictionText, "Wheel " + (int)(target.value * 10f) + "/10");
		currentConfig.friction = target.value;

		//wheelPhysics.friction = target.value;
		//wheelPhysics.bounciness = Mathf.Lerp(0.3f, 0f, target.value);
		GameController.instance.car.frictionLevel = target.value;

		GameController.instance.car.wheelFront.GetComponent<CircleCollider2D> ().enabled = false;
		GameController.instance.car.wheelRear.GetComponent<CircleCollider2D> ().enabled = false;
		GameController.instance.car.wheelFront.GetComponent<CircleCollider2D> ().enabled = true;
		GameController.instance.car.wheelRear.GetComponent<CircleCollider2D> ().enabled = true;
	}

	public void ChangeFlipAdjust (Slider target) {
		if (target == null)
			return;
		
		UIController.instance.SetText (UIController.instance.flipAdjustText, "Flip Assist " + (int)(target.value * 10f) + "/10");
		currentConfig.flipAdjust = target.value;

		//GameController.instance.car.flipAdjustment = target.value;
		GameController.instance.car.flipAssistLevel = target.value;
	}

	public void ChangeConfig (Dropdown target) {
		if (target == null)
			return;
		
		if (target.value == 0) {
			PlayerPrefs.SetInt ("Config Index", target.value);
			ApplyCarConfig (defaultConfig);
		} else {
			if (target.value != initialConfigIndex && Serializer.Load<CarConfig> (Application.persistentDataPath + "/config_-1") != null) {
				File.Delete (Application.persistentDataPath + "/config_-1");
			}
			
			LoadCarConfig (target.value);
		}
	}

	public void UnsavedConfig () {
		Serializer.Save<CarConfig> (Application.persistentDataPath + "/config_-1", currentConfig);
	}
}
