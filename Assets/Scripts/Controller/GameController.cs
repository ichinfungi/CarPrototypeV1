﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public static GameController instance;

	#region Static variables
	// tag
	public static string pickUpBoost = "Boost";
	public static string pickUpFuel = "Fuel";
	public static string pickUpHealth = "Health";
	public static string surfaceRoad = "Road";
	public static string surfaceAccelerator = "Accelerator";
	public static string surfaceMovingPlatform = "Moving Platform";
	public static string surfaceLava = "Lava";
	#endregion

	public CarController car;
	public Transform ghost;
	[HideInInspector]
	public Transform currentGround;

	[HideInInspector]
	public bool exploded;
	public GameObject explosionPrefab;

	[HideInInspector]
	public List<Vector3> fastestRecord = new List<Vector3>();
	[HideInInspector]
	public List<Vector3> currentRecord = new List<Vector3>();

	[HideInInspector]
	public float startTime = -1;
	private float lerpStart = -1;
	private float recordInterval = 0.1f;
	[HideInInspector]
	public Vector3 previousTargetRecord, nextTargetRecord;
	[HideInInspector]
	public int fastestRecordIndex = -1;
	[HideInInspector]
	public bool recordStarted = false;
	private bool skipRecord = false;

	private Vector3 groundPositionPivot = new Vector3 (267.28f, -14.6f, 0);

	[HideInInspector]
	public bool damageEnabled = true, extraWheel = false, rollerCoaster = true;

	public static string rollerCoasterPrefsKey = "Roller Coaster";
	private string damagePrefsKey = "Damage";
	private string extraWheelPrefsKey = "Extra Wheel";

	public GameObject[] carPrefabs;
	private GameObject currentCarPrefab;

	private float rewindTimestamp;
	private List<Vector3> rewindPosition = new List<Vector3>();
	private List<Quaternion> rewindRotation = new List<Quaternion>();

	public int modelDirection = -1;
	public PhysicsMaterial2D carBodyPhysics;

	[HideInInspector]
	public float carBodyHeight;

	void Awake () {
		instance = this;
	}

	// Use this for initialization
	void Start () {
		if (PlayerPrefs.GetFloat ("Fastest") == 0)
			PlayerPrefs.SetFloat ("Fastest", float.PositiveInfinity);
		else if (PlayerPrefs.GetFloat ("Fastest") != float.PositiveInfinity) {
			UIController.instance.SetText (UIController.instance.fastestTimeText, FormatTime (PlayerPrefs.GetFloat ("Fastest")));
		}

		if (PlayerPrefs.GetFloat ("Furthest") >= 0)
			UIController.instance.SetText (UIController.instance.bestText, PlayerPrefs.GetFloat ("Furthest").ToString ("0.0") + "M");

		if (PlayerPrefsX.GetVector3Array ("Fastest Record").Length > 0)
			fastestRecord = PlayerPrefsX.GetVector3Array ("Fastest Record").Cast<Vector3> ().ToList ();
		else if (PlayerPrefsX.GetVector3Array ("Furthest Record").Length > 0)
			fastestRecord = PlayerPrefsX.GetVector3Array ("Furthest Record").Cast<Vector3> ().ToList ();

		rollerCoaster = (PlayerPrefs.GetInt (rollerCoasterPrefsKey) >= 0 ? false : true);
		damageEnabled = (PlayerPrefs.GetInt (damagePrefsKey) >= 0 ? true : false);
		extraWheel = (PlayerPrefs.GetInt (extraWheelPrefsKey) > 0 ? true : false);
		ToggleDamage (damageEnabled);
		ToggleExtraWheel (extraWheel);

		SceneManager.LoadScene ("Batman", LoadSceneMode.Additive);
	}
	
	// Update is called once per frame
	void Update () {
		if (startTime >= 0)
			UIController.instance.SetText (UIController.instance.currentTimeText, FormatTime (Time.time - startTime));

		if (Time.time - rewindTimestamp > 1f) {
			rewindTimestamp = Time.time;
			rewindPosition.Add (car.transform.position);
			rewindRotation.Add (car.transform.rotation);
			if (rewindPosition.Count > 50)
				rewindPosition.RemoveAt (0);
			if (rewindRotation.Count > 50)
				rewindRotation.RemoveAt (0);
		}

		UpdateGhost ();
	}

	public static string FormatTime (float time) {
		string result = "";
		int min = (int)(time / 60);
		int second = (int)(time - 60f * min);
		int mSecond = (int)((time - min * 60f - second) * 100f);
		result = min.ToString ("00") + ":" + second.ToString ("00") + ":" + mSecond.ToString ("00");
		return result;
	}

	public void PauseGame () {
		Time.timeScale = Time.timeScale > 0 ? 0 : 1;
	}

	public void RestartGame () {
		ConfigController.instance.UnsavedConfig ();
		//Application.LoadLevel (Application.loadedLevelName);
		SceneManager.LoadScene ("Main");
	}

	public void RewindGame () {
		int index = Mathf.Clamp (rewindPosition.Count - 6, 0, int.MaxValue);
		if (rewindPosition.Count > 0) {
			car.transform.position = rewindPosition [index];
			rewindPosition.RemoveRange (index, rewindPosition.Count - index);
		}
		if (rewindRotation.Count > 0) {
			car.transform.rotation = rewindRotation [index];
			rewindRotation.RemoveRange (index, rewindRotation.Count - index);
		}
	}

	public void ClearRecord () {
		PlayerPrefs.SetFloat ("Fastest", float.PositiveInfinity);
		List<Vector3> temp = new List<Vector3> ();
		PlayerPrefsX.SetVector3Array ("Fastest Record", temp.ToArray ());
		PlayerPrefs.SetFloat ("Furthest", 0);
		PlayerPrefsX.SetVector3Array ("Furthest Record", temp.ToArray ());
	}

	IEnumerator Record () {
		WaitForSeconds recordWait = new WaitForSeconds(recordInterval);
		while (true) {
			if (!skipRecord) {
				if (fastestRecordIndex < fastestRecord.Count) {
					MoveToNextTarget ();
					UpdateGhost ();
				}
			} else
				skipRecord = false;

			currentRecord.Add (new Vector3 (car.transform.position.x - currentGround.position.x, car.transform.position.y - currentGround.position.y, car.transform.eulerAngles.z));
			yield return recordWait;
		}
	}

	private void UpdateGhost () {
		if (ghost == null)
			return;

		if (nextTargetRecord != Vector3.zero) {
			float lerp = (Time.time - lerpStart) / recordInterval;
			if (lerp > 0.99f) {
				if (fastestRecordIndex < fastestRecord.Count) {
					skipRecord = true;
					MoveToNextTarget ();
					lerp = 0;
				}
			}
			ghost.transform.position = Vector3.Lerp (new Vector3 (previousTargetRecord.x + currentGround.position.x, previousTargetRecord.y + currentGround.position.y, 0), new Vector3 (nextTargetRecord.x + currentGround.position.x, nextTargetRecord.y + currentGround.position.y, 0), lerp);
			Quaternion qFrom = Quaternion.Euler (new Vector3 (0, 0, previousTargetRecord.z));
			Quaternion qTo = Quaternion.Euler (new Vector3 (0, 0, nextTargetRecord.z));
			ghost.transform.rotation = Quaternion.Slerp (qFrom, qTo, lerp);

			ghost.GetComponentInChildren<Ghost> ().UpdateCamera ();
		}
	}

	private void MoveToNextTarget () {
		lerpStart = Time.time;
		previousTargetRecord = fastestRecord [fastestRecordIndex];
		fastestRecordIndex++;
		if (fastestRecordIndex < fastestRecord.Count)
			nextTargetRecord = fastestRecord [fastestRecordIndex];
	}

	public void Explode (bool recording = false) {
		if (exploded)
			return;
		exploded = true;

		if (recording)
			RecordFurthest ();

		Vector3 explosionPos = car.transform.position + new Vector3(Random.Range(-0.5f, 0.5f), -1f, Random.Range(-0.5f, 0.5f));
		float explosionForce = 100;
		float radius = 4f;

		car.gameObject.SetActive (false);
		GameObject go = GameObject.Instantiate (currentCarPrefab) as GameObject;
		Rigidbody rb = go.AddComponent<Rigidbody> ();
		rb.mass = car.rigidbodyComponent.mass;
		rb.constraints = RigidbodyConstraints.FreezePositionZ;
		go.AddComponent<BoxCollider> ();

		foreach (Transform t in go.GetComponentsInChildren<Transform>()) {
			if (t == go.transform)
				continue;

			t.parent = null;
			if (t.name.ToLower().Contains ("_f")) {
				if (t.name.ToLower().Contains ("_fl")) {
					t.position = car.wheelFront.transform.TransformPoint(car.wheelFront.wheelLeft.position);
					t.rotation = car.wheelFront.wheelLeft.rotation;
				} else if (t.name.ToLower().Contains ("_fr")) {
					t.position = car.wheelFront.transform.TransformPoint(car.wheelFront.wheelRight.position);
					t.rotation = car.wheelFront.wheelRight.rotation;
				}
			} else if (t.name.ToLower().Contains ("_r")) {
				if (t.name.ToLower().Contains ("_rl")) {
					t.position = car.wheelRear.transform.TransformPoint(car.wheelRear.wheelLeft.localPosition);
					t.rotation = car.wheelRear.wheelLeft.rotation;
				} else if (t.name.ToLower().Contains ("_rr")) {
					t.position = car.wheelRear.transform.TransformPoint(car.wheelRear.wheelRight.localPosition);
					t.rotation = car.wheelRear.wheelRight.rotation;
				}
			}

			rb = t.gameObject.AddComponent<Rigidbody> ();
			rb.mass = 20;
			t.gameObject.AddComponent<BoxCollider> ();
		}

		go.transform.position = car.transform.position;
		go.transform.rotation = Quaternion.AngleAxis(modelDirection * -90, car.transform.up) * car.transform.rotation;
		go.GetComponent<Rigidbody> ().AddExplosionForce (explosionForce, explosionPos, radius);
		//go.GetComponent<Rigidbody> ().AddTorque (0, 5000f, 5000f);

		/*go = GameObject.Instantiate (carWheelPrefab) as GameObject;
		go.transform.position = car.wheelFront.transform.TransformPoint(car.wheelFront.wheelLeft.localPosition);
		go.transform.rotation = car.wheelFront.wheelLeft.rotation;
		go.GetComponent<Rigidbody> ().AddExplosionForce (explosionForce/4f, explosionPos, radius/2f);
		go = GameObject.Instantiate (carWheelPrefab) as GameObject;
		go.transform.position = car.wheelFront.transform.TransformPoint(car.wheelFront.wheelRight.localPosition);
		go.transform.rotation = car.wheelFront.wheelRight.rotation;
		go.GetComponent<Rigidbody> ().AddExplosionForce (explosionForce/4f, explosionPos, radius/2f);
		go = GameObject.Instantiate (carWheelPrefab) as GameObject;
		go.transform.position = car.wheelRear.transform.TransformPoint(car.wheelRear.wheelLeft.localPosition);
		go.transform.rotation = car.wheelRear.wheelLeft.rotation;
		go.GetComponent<Rigidbody> ().AddExplosionForce (explosionForce/4f, explosionPos, radius/2f);
		go = GameObject.Instantiate (carWheelPrefab) as GameObject;
		go.transform.position = car.wheelRear.transform.TransformPoint(car.wheelRear.wheelRight.localPosition);
		go.transform.rotation = car.wheelRear.wheelRight.rotation;
		go.GetComponent<Rigidbody> ().AddExplosionForce (explosionForce/4f, explosionPos, radius/2f);*/

		if (explosionPrefab != null) {
			go = GameObject.Instantiate (explosionPrefab) as GameObject;
			go.transform.position = car.transform.position;
			go.SetActive (true);
		}

		Invoke ("RestartGame", 4);
	}

	public void RecordFurthest () {
		if (!recordStarted)
			return;
		recordStarted = false;

		nextTargetRecord = Vector3.zero;
		StopCoroutine ("Record");
		currentRecord.Add (new Vector3 (car.transform.position.x - currentGround.position.x, car.transform.position.y - currentGround.position.y, car.transform.eulerAngles.z));

		startTime = -1;

		float currentFurthest = PlayerPrefs.GetFloat ("Furthest");
		if (car.transform.position.x - currentGround.position.x + groundPositionPivot.x > currentFurthest) {
			currentFurthest = car.transform.position.x - currentGround.position.x + groundPositionPivot.x;
			PlayerPrefs.SetFloat ("Furthest", currentFurthest);
			PlayerPrefsX.SetVector3Array ("Furthest Record", currentRecord.ToArray ());
			UIController.instance.SetText (UIController.instance.bestText, PlayerPrefs.GetFloat ("Furthest").ToString ("0.0") + "M");
		}

	}

	public void ToggleDamage (bool b) {
		damageEnabled = b;
		UIController.instance.SetText (UIController.instance.damageText, "Damage " + (b ? "On" : "Off"));
		PlayerPrefs.SetInt (damagePrefsKey, (damageEnabled ? 1 : -1));
	}

	public void ToggleExtraWheel (bool b) {
		extraWheel = b;
		car.jointMiddle.enabled = b;
		car.wheelMiddle.gameObject.SetActive (b);
		UIController.instance.SetText (UIController.instance.extraWheelText, "Extra Wheel " + (b ? "On" : "Off"));
		PlayerPrefs.SetInt (extraWheelPrefsKey, (extraWheel ? 1 : -1));
	}

	public void SwapCar (int index = -1) {
		car.gameObject.SetActive (true);

		foreach (Transform t in car.carBodyParent.GetComponentsInChildren<Transform> ())
			if (t != car.carBodyParent)
				Destroy (t.gameObject);

		Destroy (car.wheelFront.wheelLeft.gameObject);
		Destroy (car.wheelFront.wheelRight.gameObject);
		Destroy (car.wheelRear.wheelLeft.gameObject);
		Destroy (car.wheelRear.wheelRight.gameObject);

		int carIndex;
		if (index >= 0)
			carIndex = index;
		else
			carIndex = Random.Range (0, carPrefabs.Length);

		currentCarPrefab = carPrefabs [carIndex];
		GameObject go = GameObject.Instantiate (currentCarPrefab) as GameObject;
		go.layer = LayerMask.NameToLayer ("Body");
		go.AddComponent<BoxCollider> ();
		Transform body = go.transform;
		Bounds bounds = body.GetComponent<MeshFilter> ().sharedMesh.bounds;
		body.parent = car.carBodyParent;
		body.localPosition = Vector3.zero;
		body.localEulerAngles = new Vector3 (0, modelDirection * -90, 0);
		body.gameObject.AddComponent<Car3DCollision> ();
		Rigidbody rb = body.gameObject.AddComponent<Rigidbody> ();
		rb.useGravity = false;
		rb.isKinematic = true;
		carBodyHeight = bounds.size.y;

		float radiusFront, radiusRear;
		radiusFront = radiusRear = 0;
		foreach (Transform t in body.GetComponentsInChildren<Transform>()) {
			if (t == body)
				continue;

			if (!t.name.ToLower().Contains ("_f") && !t.name.ToLower().Contains ("_r"))
				continue;

			//t.gameObject.AddComponent<SphereCollider> ();
			Vector3 localPos = t.localPosition;

			if (t.name.ToLower().Contains ("_f")) {
				radiusFront = t.GetComponent<MeshFilter> ().sharedMesh.bounds.extents.y;
				car.wheelFront.transform.localPosition = new Vector3(modelDirection * -localPos.z, localPos.y, 0);
				t.parent = car.wheelFront.transform;
				car.jointFront.anchor = new Vector2 (modelDirection * -localPos.z, -radiusFront);
				car.wheelFront.GetComponent<CircleCollider2D> ().radius = radiusFront;
				car.distanceFront.anchor = new Vector2 (modelDirection * -localPos.z, 0);
				car.distanceFront.distance = car.distanceMaxfront.distance = radiusFront;
				car.distanceMaxfront.anchor = car.distanceFront.anchor + new Vector2 (0, radiusFront);

				if (t.name.ToLower().Contains ("_fl"))
					car.wheelFront.wheelLeft = t;
				else if (t.name.ToLower().Contains ("_fr"))
					car.wheelFront.wheelRight = t;
			} else if (t.name.ToLower().Contains ("_r")) {
				radiusRear = t.GetComponent<MeshFilter> ().sharedMesh.bounds.extents.y;
				car.wheelRear.transform.localPosition = new Vector3(modelDirection * -localPos.z, localPos.y, 0);
				t.parent = car.wheelRear.transform;
				car.jointRear.anchor = new Vector2 (modelDirection * -localPos.z, -radiusRear);
				car.wheelRear.GetComponent<CircleCollider2D> ().radius = radiusRear;
				car.distanceRear.anchor = new Vector2 (modelDirection * -localPos.z, 0);
				car.distanceRear.distance = car.distanceMaxRear.distance = radiusRear;
				car.distanceMaxRear.anchor = car.distanceRear.anchor + new Vector2 (0, radiusRear);

				if (t.name.ToLower().Contains ("_rl"))
					car.wheelRear.wheelLeft = t;
				else if (t.name.ToLower().Contains ("_rr"))
					car.wheelRear.wheelRight = t;
			}

			// only need the z value of local position
			localPos = t.localPosition;
			localPos.x = localPos.y = 0;
			t.localPosition = localPos;
			t.localScale = Vector3.one;
		}

		// Fake extra wheel for any car
		car.wheelMiddle.transform.localPosition = (car.wheelFront.transform.localPosition + car.wheelRear.transform.localPosition) / 2f;
		car.jointMiddle.anchor = new Vector2((car.wheelFront.transform.localPosition.z + car.wheelRear.transform.localPosition.z) / 2f, 0);
		car.wheelMiddle.GetComponent<CircleCollider2D> ().radius = radiusFront;
		car.distanceMiddle.anchor = car.jointMiddle.anchor;
		car.distanceMiddle.distance = radiusFront;
		car.wheelMiddle.wheelLeft.localScale = new Vector3 (radiusFront * 2f, car.wheelMiddle.wheelLeft.localScale.y, radiusFront * 2f);
		car.wheelMiddle.wheelRight.localScale = new Vector3 (radiusFront * 2f, car.wheelMiddle.wheelRight.localScale.y, radiusFront * 2f);
		car.wheelMiddle.wheelLeft.localPosition = new Vector3 (car.wheelMiddle.wheelLeft.localPosition.x, car.wheelMiddle.wheelLeft.localPosition.y, car.wheelRear.wheelLeft.localPosition.z);
		car.wheelMiddle.wheelRight.localPosition = new Vector3 (car.wheelMiddle.wheelRight.localPosition.x, car.wheelMiddle.wheelRight.localPosition.y, car.wheelRear.wheelRight.localPosition.z);

		// Update car body and corresponding collider
		body.localPosition = new Vector3 (0, -radiusFront / 2f, 0);
		car.rigidbodyComponent.centerOfMass = new Vector3 (modelDirection * -bounds.center.z, 0, 0);
		car.carBodyParent.parent.GetComponent<BoxCollider2D> ().offset = new Vector2 (modelDirection * -bounds.center.z, bounds.center.y);
		car.carBodyParent.parent.GetComponent<BoxCollider2D> ().size = new Vector2 (bounds.extents.z * 2f * 0.65f, bounds.extents.y * 2f * 0.75f);

		// Update particle system
		Vector3 particleSystemPos = car.wheelFront.transform.localPosition;
		particleSystemPos.y -= radiusFront;
		particleSystemPos.z = car.wheelFront.wheelRight.transform.localPosition.z;
		car.wheelFront.particleSystem.transform.localPosition = particleSystemPos;
		particleSystemPos = car.wheelRear.transform.localPosition;
		particleSystemPos.y -= radiusRear;
		particleSystemPos.z = car.wheelRear.wheelRight.transform.localPosition.z;
		car.wheelRear.particleSystem.transform.localPosition = particleSystemPos;

		BoxCollider2D bc = car.gameObject.AddComponent<BoxCollider2D> ();
		bc.sharedMaterial = carBodyPhysics;
		bc.size = new Vector2 (radiusFront / 10f, radiusFront);
		Vector3 wheelFrontRelative = car.transform.InverseTransformPoint (car.wheelFront.transform.position);
		bc.offset = new Vector2 (wheelFrontRelative.x + radiusFront + bc.size.x / 2f, wheelFrontRelative.y / 2f);
		bc = car.gameObject.AddComponent<BoxCollider2D> ();
		bc.sharedMaterial = carBodyPhysics;
		bc.size = new Vector2 (radiusRear / 10f, radiusRear);
		Vector3 wheelRearRelative = car.transform.InverseTransformPoint (car.wheelRear.transform.position);
		bc.offset = new Vector2 (wheelRearRelative.x - radiusRear - bc.size.x / 2f, wheelRearRelative.y / 2f);
		bc.sharedMaterial = carBodyPhysics;

		bc = car.gameObject.AddComponent<BoxCollider2D> ();
		bc.sharedMaterial = carBodyPhysics;
		bc.size = new Vector2 (wheelFrontRelative.x - wheelRearRelative.x - radiusFront - radiusRear, bounds.extents.y * 2f * 0.75f);
		bc.offset = new Vector2 (modelDirection * -bounds.center.z, bounds.extents.y * 0.7f);
		bc.sharedMaterial = carBodyPhysics;
	}
}
