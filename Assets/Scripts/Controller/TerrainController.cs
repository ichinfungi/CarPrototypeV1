﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TerrainController : MonoBehaviour {

	public GameObject roadPrefab, bridgePrefab, coinPrefab;
	public Transform car;
	private Vector3 currentPostion = new Vector3(3, 0, 0);

	private List<Transform> cubes = new List<Transform>();

	private int minHillWidth = 5, maxHillWidth = 10;
	private float minHillHeight = 1, maxHillHeight = 5;

	enum TerrainPattern {
		StraightLine,
		Hill,
		Bridge
	};

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Vector3.SqrMagnitude (currentPostion - car.position) < 1000) {
			Vector3 temp = currentPostion;
			TerrainPattern pattern;
			if (Random.value > 0.4f) {
				pattern = TerrainPattern.Hill;
			} else {
				pattern = TerrainPattern.Bridge;
			}
			Vector3[] position = GeneratePattern (pattern);
			PlaceRoadPrefab (pattern, position);
			if (Random.value > 0.7f) {
				GameObject go;
				for (int i = 0; i < position.Length; i++) {
					go = GameObject.Instantiate (coinPrefab) as GameObject;
					go.transform.position = temp + new Vector3(position [i].x, position[i].y, 0) + new Vector3 (0, 1, 0);
				}
			}
		}

		if (cubes.Count > 0 && Vector3.SqrMagnitude (cubes [0].position - car.position) > 1000) {
			GameObject go = cubes [0].gameObject;
			cubes.RemoveAt (0);
			Destroy (go);
		}
	}

	private Vector3[] GeneratePattern (TerrainPattern currentPattern) {
		int number;
		List<Vector3> pattern = new List<Vector3> ();
		switch (currentPattern) {
		case TerrainPattern.Bridge:
				number = Random.Range (5, 10);
				for (int i = 0; i < number; i++) {
					pattern.Add(new Vector3(i * 2, 0, 0));
				}
				break;
		case TerrainPattern.Hill:
				number = Random.Range (minHillWidth, maxHillWidth);
				float amplitude = (float)number / maxHillWidth * maxHillHeight;
				float phase = Mathf.Deg2Rad * Random.Range (0, 180f);
				float zero = amplitude * Mathf.Sin (0 - phase);
				for (int i = 0; i < number; i++) {
					float y = amplitude * Mathf.Sin (Mathf.Deg2Rad * (i * 60f / number) - phase);
					pattern.Add (new Vector3(i * roadPrefab.transform.localScale.x, y - zero, ((number / 2 - Mathf.Abs(number / 2 - i)) / (float)(number / 2)) * maxHillWidth * (amplitude / maxHillHeight) ));
				}
				break;
		}
		return pattern.ToArray ();
	}

	private void PlaceRoadPrefab (TerrainPattern pattern, Vector3[] position) {
		List<Transform> temp = new List<Transform> ();
		GameObject go;
		for (int i = 0; i < position.Length; i++)
		{
			go = GameObject.Instantiate (roadPrefab) as GameObject;
			go.transform.position = currentPostion + new Vector3(position[i].x, position[i].y, 0);;
			if (i != 0 && i != position.Length-1) {
				float angle = Vector3.Angle (go.transform.position, temp [i - 1].transform.position);
				angle = Mathf.Rad2Deg * angle;
				Vector3 diff = go.transform.position - temp [i - 1].transform.position;
				angle = Mathf.Rad2Deg * Mathf.Tan (diff.y / diff.x);
				go.transform.RotateAround (go.transform.position, go.transform.forward, angle);
			}
			go.transform.parent = transform;
			temp.Add (go.transform);
			cubes.Add (go.transform);
		}

		if (pattern == TerrainPattern.Bridge)
		{
			for (int i = 0; i < temp.Count; i++)
			{
				temp [i].GetComponentInChildren<MeshRenderer> ().material.color = Color.gray;

				Vector3 pos = temp [i].position;
				temp[i].gameObject.AddComponent<Rigidbody2D> ();
				HingeJoint2D hinge = temp[i].gameObject.AddComponent<HingeJoint2D> ();
				hinge.enableCollision = true;
				if (i == 0)
					hinge.connectedAnchor = new Vector2 (temp[i].position.x, temp[i].position.y);
				if (i != 0) {
					hinge.connectedBody = temp [i - 1].gameObject.GetComponent<Rigidbody2D> ();
					hinge.connectedAnchor = new Vector2 ((temp [i].position.x - temp [i - 1].position.x) / temp[i].localScale.x, (temp [i].position.y - temp [i - 1].position.y));
				}
				if (i == temp.Count - 1) {
					hinge = temp [i].gameObject.AddComponent<HingeJoint2D> ();
					hinge.enableCollision = true;
					temp [i].GetComponent<Rigidbody2D> ().isKinematic = true;
					//hinge.connectedAnchor = new Vector2 (temp[i].position.x, temp[i].position.y);
				}
			}
		}
		currentPostion = cubes [cubes.Count - 1].position + new Vector3(2, 0, 0);
	}

	private void PlaceBridge () {
		GameObject go = GameObject.Instantiate (bridgePrefab) as GameObject;
		go.transform.position = currentPostion + new Vector3(8, 0, 0);
		go.transform.parent = transform;
		/*Transform[] t = go.GetComponentsInChildren<Transform> ();
		for (int i = 0; i < t.Length; i++) {
			if (t [i].position.x > currentPostion.x) {
				currentPostion = t [i].position;
			}
		}*/
	}
}
