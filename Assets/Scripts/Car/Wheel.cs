﻿using UnityEngine;
using System.Collections;

public class Wheel : MonoBehaviour {

	public ParticleSystem particleSystem;
	public Transform wheelLeft, wheelRight;
	public bool isRearWheel = false;

	public bool grounded { get; set; }
	public Vector2 contactNormal { get; set; }
	public Vector2 relativeVelocity { get; set; }
	public Vector2 contactPoint { get; set; }
	
	// Update is called once per frame
	void Update () {
		if (grounded && particleSystem != null && UserInputController.instance.inputX != 0) {
			Vector3 up = new Vector3 (contactNormal.x, contactNormal.y, 0);
			particleSystem.transform.rotation = Quaternion.LookRotation (transform.forward, up) * Quaternion.AngleAxis(-90, Vector3.up) * Quaternion.AngleAxis(-45, Vector3.right);

			//Vector3 up = (UserInputController.instance.inputX < 0? -1 : 1) * Vector3.right;
			//particleSystem.transform.rotation = Quaternion.LookRotation (new Vector3 (contactNormal.x, contactNormal.y, 0), up) * Quaternion.Euler (70, 0, 0);

			//Vector3 temp = particleSystem.transform.eulerAngles;
			//temp.y = (UserInputController.instance.inputX < 0 ? 90 : 270);
			//particleSystem.transform.eulerAngles = temp;
			if (GameController.instance.car.fuel > 0)
				particleSystem.Emit ((int)(200f * Time.deltaTime * Mathf.Abs(UserInputController.instance.inputX)));
		}
	}

	void FixedUpdate () {
		if (grounded) {
			Vector2 origin = new Vector2 (transform.position.x, transform.position.y);
			int layer = 1 << LayerMask.NameToLayer ("Road");
			RaycastHit2D hit = Physics2D.Raycast (origin, contactPoint - origin, GetComponent<CircleCollider2D>().radius, layer);
			if (hit != null) {
				if (hit.distance < GetComponent<CircleCollider2D> ().radius * 0.9f) {
					Vector2 targetPosition = contactPoint + contactNormal * GetComponent<CircleCollider2D> ().radius;
					//GetComponent<Rigidbody2D> ().MovePosition (targetPosition);
					transform.position = new Vector3 (targetPosition.x, targetPosition.y);
				}
			}
		}
	}

	void OnCollisionEnter2D (Collision2D coll) {
		if (coll.transform.tag == GameController.surfaceRoad || coll.transform.tag == GameController.surfaceMovingPlatform)
			grounded = true;

		if (coll.transform.tag == GameController.surfaceLava)
			GameController.instance.car.ChangeHealth (-100);

		UpdateCollisionData (coll);

		if (isRearWheel)
			if (coll.transform.tag == GameController.surfaceMovingPlatform)
				coll.transform.GetComponent<MovingPlatform> ().ActivatePlatform ();
	}

	void OnCollisionExit2D (Collision2D coll) {
		if (coll.transform.tag == GameController.surfaceRoad || coll.transform.tag == GameController.surfaceMovingPlatform)
			grounded = false;

		UpdateCollisionData (coll);
	}

	void OnCollisionStay2D (Collision2D coll) {
		UpdateCollisionData (coll);
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == GameController.surfaceAccelerator && GameController.instance != null)
			if (!isRearWheel)
				GameController.instance.car.wheelFrontAccelerator = true;
			else
				GameController.instance.car.wheelRearAccelerator = true;
	}

	void OnTriggerExit2D (Collider2D other) {
		if (other.tag == GameController.surfaceAccelerator && GameController.instance != null)
			if (!isRearWheel)
				GameController.instance.car.wheelFrontAccelerator = false;
			else
				GameController.instance.car.wheelRearAccelerator = false;
	}

	void OnTriggerStay2D (Collider2D other) {
		
	}

	void UpdateCollisionData (Collision2D coll) {
		contactNormal = coll.contacts [0].normal;
		relativeVelocity = coll.relativeVelocity;
		contactPoint = coll.contacts [0].point;
	}
}
