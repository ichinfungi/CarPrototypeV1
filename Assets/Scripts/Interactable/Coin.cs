﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Coin : MonoBehaviour {

	private float _animationTimer = -1, _animationDuration = 2;
	private bool _hitBefore;
	private Transform score;

	// Update is called once per frame
	void Update () {
		// when coin is eaten, it move upwards and rotate around itself
		if (_animationTimer > -1)
		{
			_animationTimer += Time.deltaTime;
			transform.Translate(Vector3.up * Time.deltaTime * 3f);
			transform.Rotate(Vector3.up * Time.deltaTime * 360f);

			score.Translate(Vector3.up * Time.deltaTime * 3f);

			if (_animationTimer > _animationDuration) {
				EventController.instance.props.Remove (this.transform);
				Destroy (score.gameObject);
				Destroy (this.gameObject);
			}
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.transform.root.name.Contains("Car"))
		{
			// checking _hitBefore to avoid the self rotation to trigger second time
			if (!_hitBefore)
			{
				_hitBefore = true;
				_animationTimer = 0;
				AudioController.instance.PlayCoinSFX ();

				score = (GameObject.Instantiate (UIController.instance.coinScorePrefab) as GameObject).transform;
				score.SetParent(UIController.instance.worldCanvas.transform);
				score.position = transform.position + new Vector3(0, 3f, 0);
			}
		}
	}
}
